const models = require('../models');

const fetchWinners = async (req, res) => {};

const findWinner = async (battleId, nftId) => {
  return await models.Winner.findOne({
    where: {
      battleId,
      nftId,
    },
  });
};

const addWinner = async (payload) => {
  return await models.Winner.findOrCreate({
    where: {
      battleId: payload.battleId,
      nftId: payload.nftId,
    },
    defaults: payload,
  });
};

const updateWinner = async (payload) => {
  return await models.Winner.update(payload, {
    where: {
      battleId: payload.battleId,
      nftId: payload.nftId,
    },
  });
};


module.exports = {
  // For rest api
  fetchWinners,

  // For worker
  findWinner,
  addWinner,
  updateWinner,
};
