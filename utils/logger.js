const Logger = require('node-color-log');

const info = (str) => {
  Logger.info(str);
};

const debug  = (str) => {
  Logger.debug(str);
};

const warning = (str) => {
  Logger.warn(str);
};

const error = (str) => {
  Logger.error(str);
};

module.exports = {
  info,
  debug,
  warning,
  error,
};
