const {
  getProvider,
  getBattleInstance,
} = require('../contracts');

const battle = require('./battle');

require('dotenv').config();

const initialize = async () => {
  const chainId = process.env.CHAIN_ID;

  const provider = await getProvider(chainId);
  const instance = await getBattleInstance(chainId, provider);

  // Load all data from contract
  await battle.initialize(instance, provider, chainId);

  await addEventListeners(instance);
};

const addEventListeners = async (instance) => {
  instance.events.NewBattleCreated().on('data', (event) => {
    battle.handleNewBattleCreated([event]);
  });

  instance.events.NewParticipantJoined().on('data', (event) => {
    battle.handleNewParticipantJoined([event]);
  });

  instance.events.NewWinner().on('data', (event) => {
    battle.handleNewWinner([event]);
  });

  instance.events.ClaimedPresent().on('data', (event) => {
    battle.handleClaimedPresent([event]);
  });
};

module.exports = {
  initialize,
};
