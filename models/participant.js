'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Participant extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Participant.belongsTo(models.Battle, {
        foreignKey: 'battleId',
        as: 'battle',
      });
    }
  };
  Participant.init({
    battleId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    nftId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    nftOwner: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    isClaimed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    hasInsurance: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
  }, {
    scopes: {
      withBattle: () => ({
        include: [
          {
            as: 'battle',
            model: sequelize.model('Battles'),
          },
        ],
      })
    },
    sequelize,
    modelName: 'Participant',
  });
  return Participant;
};