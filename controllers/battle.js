const models = require('../models');

const fetchBattles = async (req, res) => {
  try {
    const battles = await models.Battle.scope('withWinner').findAndCountAll({
      order: [
        ['id', 'desc'],
      ],
    });

    return res.status(200).json(battles);
  } catch (error) {
    return res.status(500).json({
      msg: (error && error.message) || 'Interval Error',
    });
  }
};

const fetchBattlesByLevel = async (req, res) => {
  try {
    const { level } = {
      level: 5,
      ...(req.query || {}),
    };
    const battles = {};

    for (let i=1; i<=level; i++) {
      const battle = await models.Battle.scope(['onlyLiveBattles']).findOne({
        where: {
          level: i
        },
        order: [
          ['id', 'desc'],
        ],
      });

      battles[i] = battle;
    }

    return res.status(200).json(battles);
  } catch (error) {
    return res.status(500).json({
      msg: (error && error.message) || 'Interval Error',
    });
  }
};

const fetchBattleHistories = async (req, res) => {
  const { limit } = {
    limit: 5,
    ...(req.query || {}),
  };

  try {
    const battles = await models.Battle.scope(['onlyEndedBattles', 'withWinner']).findAndCountAll({
      limit,
    });

    return res.status(200).json(battles);
  } catch (error) {
    return res.status(500).json({
      msg: (error && error.message) || 'Interval Error',
    });
  }
};

const findBattle = async (battleId) => {
  return await models.Battle.findByPk(battleId);
};

const addBattle = async (payload) => {
  return await models.Battle.findOrCreate({
    where: {
      id: payload.id,
    },
    defaults: payload,
  });
};

const updateBattle = async (payload) => {
  return await models.Battle.update(payload, {
    where: {
      id: payload.id,
    },
  });
};

module.exports = {
  // For API
  findBattle,
  fetchBattles,
  fetchBattlesByLevel,
  fetchBattleHistories,

  // For Worker
  addBattle,
  updateBattle,
};
