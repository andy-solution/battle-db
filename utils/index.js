const { ethers } = require('ethers');

const formatId = (str) => {
  return ethers.utils.id(str);
};

module.exports = {
  formatId,
};
