const express = require('express');
const battles = require('../controllers/battle');

const router = express.Router();

router.get('/', battles.fetchBattles);
router.get('/live', battles.fetchBattlesByLevel);
router.get('/history', battles.fetchBattleHistories);

module.exports = router;
