const { ethers } = require('ethers');
const rpcs = require('./rpcs');
const address = require('./address');

const battleABI = require('./abis/battle.json');

const getProvider = (chainId = 137) => {
  if (!rpcs[chainId]) {
    console.log('Unsupported chainId');
    return;
  }

  console.log('DEBUG-networks', ethers.providers);

  return new ethers.providers.JsonRpcProvider(rpcs[chainId], chainId);
};

const getBattleInstance = (chainId = 137, provider) => {
  if (!address.battle[chainId]) {
    console.log('Unsupported chainId');
    return;
  }

  if (!provider) {
    console.log('No provider');
    return;
  }

  return new ethers.Contract(address.battle[chainId], battleABI, provider);
};

module.exports = {
  getProvider,
  getBattleInstance,
};
