module.exports = {
  1: 'https://mainnet.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161',
  56: 'https://bsc-dataseed1.ninicoin.io',
  97: 'https://data-seed-prebsc-1-s1.binance.org:8545',
  137: 'https://polygon-rpc.com',
  // 137: 'wss://rpc-mainnet.maticvigil.com/ws/v1/0799d5bc18a0549f82ef51f9a597b6ccda1de2b9',
  80001: 'https://rpc-mumbai.matic.today',
  128: 'https://http-mainnet.hecochain.com',
  256: 'https://http-testnet.hecochain.com',
};
