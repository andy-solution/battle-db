const Web3 = require('web3');
const rpcs = require('./rpcs');
const address = require('./address');

const battleABI = require('./abis/battle.json');

const getProvider = (chainId = 137) => {
  if (!rpcs[chainId]) {
    console.log('Unsupported chainId');
    return;
  }

  return new Web3(rpcs[chainId]);
};

const getBattleInstance = (chainId = 137, provider) => {
  if (!address.battle[chainId]) {
    console.log('Unsupported chainId');
    return;
  }

  if (!provider) {
    console.log('No provider');
    return;
  }

  return new provider.eth.Contract(battleABI, address.battle[chainId], provider);
};

module.exports = {
  getProvider,
  getBattleInstance,
};
