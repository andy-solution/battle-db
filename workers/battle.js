const address = require('../contracts/address');
const blocks = require('../contracts/blocks');

const BattleController = require('../controllers/battle');
const WinnerController = require('../controllers/winner');

const Logger = require('../utils/logger');

const initialize = async (instance, provider, chainId) => {
  Logger.info("<<<<-- Initializing Battle Worker -->>>>>");

  const step = 10000;
  const lastestBlockNumber = await provider.eth.getBlockNumber();
  let currentBlockNumber = blocks.battle[chainId] + 1;
  // const lastestBlockNumber = 18562660;
  // let currentBlockNumber = 18552660;
  let participants = {};

  while (currentBlockNumber < lastestBlockNumber) {
    const filter = {
      fromBlock: currentBlockNumber,
      toBlock: currentBlockNumber + step,
    };

    Logger.debug(`<<<<-- Filtering between <${filter.fromBlock} - ${filter.toBlock}> blocks -->>>>>`);

    try {
      // NewBattleCreated
      Logger.info("<<<<-- NewBattleCreated -->>>>>");
      const battleCreatedEvents = await instance.getPastEvents('NewBattleCreated', filter);
      Logger.debug(`<<<<-- ${battleCreatedEvents.length} NewBattleCreated -->>>>>`);
      await handleNewBattleCreated(battleCreatedEvents);

      // NewParticipantJoined
      Logger.info("<<<<-- NewParticipantJoined -->>>>>");
      const participantsEvents = await instance.getPastEvents('NewParticipantJoined', filter);
      Logger.debug(`<<<<-- ${participantsEvents.length} NewParticipantJoined -->>>>>`);
      participantsEvents.forEach(event => {
        const battleId = +event.returnValues.battleId;
        if (!participants[battleId]) {
          participants[battleId] = 0;
        }

        participants[battleId] = +event.returnValues.total;
      });

      // NewWinner
      Logger.info("<<<<-- NewWinner -->>>>>");
      const winnerEvents = await instance.getPastEvents('NewWinner', filter);
      Logger.debug(`<<<<-- ${winnerEvents.length} NewWinner -->>>>>`);
      await handleNewWinner(winnerEvents);
      
      // ClaimedPresent
      Logger.info("<<<<-- ClaimedPresent -->>>>>");
      const claimedPresentEvents = await instance.getPastEvents('ClaimedPresent', filter);
      Logger.debug(`<<<<-- ${claimedPresentEvents.length} ClaimedPresent -->>>>>`);
      await handleClaimedPresent(claimedPresentEvents);
      
    } catch (error) {
      Logger.error("##################### ERROR #######################");
      Logger.error(error.message);
    }

    currentBlockNumber += step;
    currentBlockNumber = Math.min(currentBlockNumber + 1, lastestBlockNumber);
  }

  // NewParticipantJoined
  for (const battleId of Object.keys(participants)) {
    if (!participants[battleId]) continue;

    const payload = {
      id: battleId,
      total: participants[battleId],
    };

    Logger.warning(payload);

    await BattleController.updateBattle(payload);
  }

  Logger.info("<<<<-- Ended Initializing Battle Worker -->>>>>");
};

const handleNewBattleCreated = async (battleCreatedEvents) => {
  for (const event of battleCreatedEvents) {
    const payload = {
      id: +event.returnValues.battleId,
      uri: event.returnValues.uri,
      level: +event.returnValues.level,
      startTime: (+event.returnValues.startTime) * 1000,
      endTime: (+event.returnValues.endTime) * 1000,
      award: +event.returnValues.award,
      status: 0,
      total: 0,
    };

    Logger.warning(payload);

    await BattleController.addBattle(payload);
  }
};

const handleNewParticipantJoined = async (participants) => {
  for (const event of participants) {
    const payload = {
      battleId: +event.returnValues.battleId,
      total: +event.returnValues.total,
    };

    Logger.warning(payload);

    await BattleController.updateBattle(payload);
  }
};

const handleClaimedPresent = async (claimedPresentEvents) => {
  for (const event of claimedPresentEvents) {
    const payload = {
      battleId: +event.returnValues.battleId,
      nftId: +event.returnValues.nftId,
      isClaimed: true,
    };

    Logger.warning(payload);

    await WinnerController.updateWinner(payload);
  }
};

const handleNewWinner = async (winnerEvents) => {
  for (const event of winnerEvents) {
    const payload = {
      battleId: +event.returnValues.battleId,
      nftId: +event.returnValues.nftId,
      nftOwner: event.returnValues.nftOwner,
      isClaimed: false,
    };

    Logger.warning(payload);

    await WinnerController.addWinner(payload);

    const data = {
      id: +event.returnValues.battleId,
      status: 1,
    };

    await BattleController.updateBattle(data);
  }
};

module.exports = {
  initialize,
  handleNewBattleCreated,
  handleNewParticipantJoined,
  handleClaimedPresent,
  handleNewWinner,
};
