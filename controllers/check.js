const checkHealth = (req, res) => {
  res.status(200).json({
    message: 'Health checking...',
  });
};

module.exports = {
  checkHealth,
};
