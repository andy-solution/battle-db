'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Battle extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Battle.hasMany(models.Participant, {
        foreignKey: "battleId",
        as: "participants",
      });

      Battle.hasOne(models.Winner, {
        foreignKey: "battleId",
        as: "winner",
      });
    }
  };
  Battle.init({
    uri: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    startTime: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    endTime: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    level: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    award: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    total: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
  }, {
    scopes: {
      onlyLiveBattles: () => ({
        where: {
          status: 0,
        },
      }),
      onlyEndedBattles: () => ({
        where: {
          status: 1,
        }
      }),
      withParticipants: () => ({
        include: [
          {
            as: 'participants',
            model: sequelize.model('Participant'),
          },
        ],
      }),
      withWinner: () => ({
        include: [
          {
            as: 'winner',
            model: sequelize.model('Winner'),
          },
        ],
      }),
    },
    sequelize,
    modelName: 'Battle',
  });
  return Battle;
};
