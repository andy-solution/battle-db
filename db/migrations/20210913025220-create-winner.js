'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Winners', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      battleId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Battles',
          key: 'id',
        },
      },
      nftId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      nftOwner: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      isClaimed: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Winners');
  }
};