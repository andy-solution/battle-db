const express = require('express');
const check = require('../controllers/check');

const router = express.Router();

router.get('/', check.checkHealth);

module.exports = router;
