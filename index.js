const http = require("http");
const Logger = require('./utils/logger');

const app = require('./routes');
const worker = require('./workers');

require("dotenv").config();

const server = http.createServer(app);
const port = process.env.SERVER_PORT || 5000;

server.listen(port, async () => {
  Logger.info(`Server is running on ${port}`);

  Logger.info('============ Initializing Worker ============');
  await worker.initialize();
  Logger.info('============ Initializing Worker Ended ============');
});
