const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');

const battle = require('./battle');
const check = require('./check');;

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.use('/check', check);
app.use('/battle', battle);

module.exports = app;
